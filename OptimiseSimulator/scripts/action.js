﻿var ServiceURL = "https://optimise.marstongroup.co.uk/OptimiseAPI/";
var DataType = 'json';
var objcnt = 0;
var actionbuttons = "";
var officeridarray = ["114", "4289", "779"];
var passwordarray = ["2546", "9581", "7651"];
var officercount = 0;
var hbindex = 0;
var autoactioninterval=0;

var funclist = new Array();
var methodlist = new Array();
var paramlist = new Array();

var doorcolor = new Array();
doorcolor[0] = "Aluminium";
doorcolor[1] = "Black";
doorcolor[2] = "Blue";
doorcolor[3] = "Brown";
doorcolor[4] = "Glass";
doorcolor[5] = "Green";
doorcolor[6] = "Not Seen";
doorcolor[7] = "Red";
doorcolor[8] = "Security Door";
doorcolor[9] = "Shutter";
doorcolor[10] = "Wood";
doorcolor[11] = "White";
doorcolor[12] = "Yellow";
doorcolor[13] = "ANPR Onstreet";
doorcolor[14] = "ANPR Road side";

var housetype = new Array();
housetype[0] = "B and B";
housetype[1] = "Bungalow";
housetype[2] = "Caravan";
housetype[3] = "Cottage";
housetype[4] = "Detached";
housetype[5] = "Flat";
housetype[6] = "Hostel";
housetype[7] = "Maisonette";
housetype[8] = "Mobile Home";
housetype[9] = "Not Seen";
housetype[10] = "Offices";
housetype[11] = "Pub";
housetype[12] = "Restaurant/Take a";
housetype[13] = "Semi Detached";
housetype[14] = "Shop";
housetype[15] = "Terraced";
housetype[16] = "Warehouse";
housetype[17] = "ANPR Onstreet";
housetype[18] = "ANPR Road side";

var gpstime = new Array();
gpstime[0]  = "12:01:15";
gpstime[1]  = "12:08:15";
gpstime[2]  = "12:15:15";
gpstime[3]  = "12:22:15";
gpstime[4]  = "12:29:15";
gpstime[5]  = "12:36:15";
gpstime[6]  = "12:43:15";
gpstime[7]  = "12:50:15";
gpstime[8]  = "12:57:15";
gpstime[9]  = "01:04:15";
gpstime[10] = "01:11:15";
gpstime[11] = "01:18:15";
gpstime[12] = "01:25:15";
gpstime[13] = "01:32:15";
gpstime[14] = "01:39:15";
gpstime[15] = "01:46:15";
gpstime[16] = "01:53:15";
gpstime[17] = "02:00:15";
gpstime[18] = "02:07:15";
gpstime[19] = "02:14:15";
gpstime[20] = "02:21:15";
gpstime[21] = "02:28:15";
gpstime[22] = "02:35:15";
gpstime[23] = "02:42:15";
gpstime[24] = "02:49:15";
gpstime[25] = "02:56:15";
gpstime[26] = "03:03:15";
gpstime[27] = "03:10:15";
gpstime[28] = "03:17:15";
gpstime[29] = "03:24:15";
gpstime[30] = "03:31:15";
gpstime[31] = "03:38:15";
gpstime[32] = "03:45:15";
gpstime[33] = "03:52:15";
gpstime[34] = "03:59:15";
gpstime[35] = "04:06:15";
gpstime[36] = "04:13:15";
gpstime[37] = "04:20:15";
gpstime[38] = "04:27:15";
gpstime[39] = "04:34:15";
gpstime[40] = "04:41:15";
gpstime[41] = "04:48:15";
gpstime[42] = "04:55:15";
gpstime[43] = "05:02:15";
gpstime[44] = "05:09:15";
gpstime[45] = "05:16:15";
gpstime[46] = "05:23:15";
gpstime[47] = "05:30:15";
gpstime[48] = "05:37:15";
gpstime[49] = "05:44:15";
gpstime[50] = "05:51:15";
gpstime[51] = "05:58:15";
gpstime[52] = "06:05:15";
gpstime[53] = "06:12:15";
gpstime[54] = "06:19:15";
gpstime[55] = "06:26:15";
gpstime[56] = "06:33:15";
gpstime[57] = "06:40:15";
gpstime[58] = "06:47:15";
gpstime[59] = "06:54:15";
gpstime[60] = "07:01:15";
gpstime[61] = "07:08:15";
gpstime[62] = "07:15:15";
gpstime[63] = "07:22:15";
gpstime[64] = "07:29:15";
gpstime[65] = "07:36:15";
gpstime[66] = "07:43:15";
gpstime[67] = "07:50:15";
gpstime[68] = "07:57:15";
gpstime[69] = "08:04:15";
gpstime[70] = "08:11:15";
gpstime[71] = "08:18:15";
gpstime[72] = "08:25:15";
gpstime[73] = "08:32:15";
gpstime[74] = "08:39:15";
gpstime[75] = "08:46:15";
gpstime[76] = "08:53:15";
gpstime[77] = "09:00:15";
gpstime[78] = "09:07:15";
gpstime[79] = "09:14:15";
gpstime[80] = "09:21:15";
gpstime[81] = "09:28:15";
gpstime[82] = "09:35:15";
gpstime[83] = "09:42:15";
gpstime[84] = "09:49:15";
gpstime[85] = "09:56:15";
gpstime[86] = "10:03:15";
gpstime[87] = "10:10:15";
gpstime[88] = "10:17:15";
gpstime[89] = "10:24:15";
gpstime[90] = "10:31:15";
gpstime[91] = "10:38:15";
gpstime[92] = "10:45:15";
gpstime[93] = "10:52:15";
gpstime[94] = "10:59:15";
gpstime[95] = "11:06:15";
gpstime[96] = "11:13:15";
gpstime[97] = "11:20:15";
gpstime[98] = "11:27:15";
gpstime[99] = "11:34:15";

var langarray = new Array();
langarray[0] = "-0.32667";
langarray[1] = "-0.336735";
langarray[2] = "-0.32829";
langarray[3] = "-0.315551";
langarray[4] = "-0.311594";
langarray[5] = "-0.304158";
langarray[6] = "-0.298252";
langarray[7] = "-0.33838";
langarray[8] = "-0.35496";
langarray[9] = "-0.344769";
langarray[10] = "-0.335986";
langarray[11] = "-0.338072";
langarray[12] = "-0.385866";
langarray[13] = "-0.440252";
langarray[14] = "-0.288044";
langarray[15] = "-0.297019";
langarray[16] = "-0.303264";
langarray[17] = "-0.340937";
langarray[18] = "-0.359453";
langarray[19] = "-0.375739";
langarray[20] = "-0.351273";
langarray[21] = "-0.341607";
langarray[22] = "-0.341035";
langarray[23] = "-0.183581";
langarray[24] = "-0.216314";
langarray[25] = "-0.190296";
langarray[26] = "-0.173023";
langarray[27] = "-0.191913";
langarray[28] = "-0.192171";
langarray[29] = "-0.196579";
langarray[30] = "-0.210575";
langarray[31] = "-0.209996";
langarray[32] = "-0.212106";
langarray[33] = "-0.170714";
langarray[34] = "-0.210272";
langarray[35] = "-0.227756";
langarray[36] = "-0.226209";
langarray[37] = "-0.226863";
langarray[38] = "-0.240163";
langarray[39] = "-1.90356";
langarray[40] = "-1.91067";
langarray[41] = "-1.90952";
langarray[42] = "-1.91298";
langarray[43] = "-1.89541";
langarray[44] = "-1.8985";
langarray[45] = "-1.89969";
langarray[46] = "-1.90525";
langarray[47] = "-1.90028";
langarray[48] = "-1.90305";
langarray[49] = "-1.89713";
langarray[50] = "-1.89019";
langarray[51] = "-1.89802";
langarray[52] = "-1.88971";
langarray[53] = "-1.89334";
langarray[54] = "-1.90228";
langarray[55] = "-1.89127";
langarray[56] = "-1.88638";
langarray[57] = "-1.89142";
langarray[58] = "-1.87763";
langarray[59] = "-1.89421";
langarray[60] = "-1.87942";
langarray[61] = "-1.86853";
langarray[62] = "-1.85664";
langarray[63] = "-1.8316";
langarray[64] = "-1.84245";
langarray[65] = "-1.86762";
langarray[66] = "-1.84235";
langarray[67] = "-1.85196";
langarray[68] = "-1.86";
langarray[69] = "-1.84617";
langarray[70] = "-1.87027";
langarray[71] = "-1.8496";
langarray[72] = "-1.85086";
langarray[73] = "-1.86606";
langarray[74] = "-1.86312";
langarray[75] = "-1.88239";
langarray[76] = "-1.87741";
langarray[77] = "-1.8884";
langarray[78] = "-1.8739";
langarray[79] = "-1.8911";
langarray[80] = "-1.89376";
langarray[81] = "-1.87708";
langarray[82] = "-1.87304";
langarray[83] = "-1.89318";
langarray[84] = "-1.90119";
langarray[85] = "-1.89403";
langarray[86] = "-1.91751";
langarray[87] = "-1.91963";
langarray[88] = "-1.93342";
langarray[89] = "-1.9405";
langarray[90] = "-1.93025";
langarray[91] = "-1.92482";
langarray[92] = "-1.93805";
langarray[93] = "-1.95874";
langarray[94] = "-1.96931";
langarray[95] = "-1.96103";
langarray[96] = "-1.93799";
langarray[97] = "-1.92301";
langarray[98] = "-1.91275";
langarray[99] = "-1.92709";

var latarray = new Array();
latarray[0] = "51.7448";
latarray[1] = "51.7404";
latarray[2] = "51.752";
latarray[3] = "51.7564";
latarray[4] = "51.747";
latarray[5] = "51.7482";
latarray[6] = "51.7252";
latarray[7] = "51.7222";
latarray[8] = "51.7206";
latarray[9] = "51.7489";
latarray[10] = "51.7579";
latarray[11] = "51.7677";
latarray[12] = "51.7905";
latarray[13] = "51.8221";
latarray[14] = "51.7475";
latarray[15] = "51.809";
latarray[16] = "51.7678";
latarray[17] = "51.8066";
latarray[18] = "51.8108";
latarray[19] = "51.8249";
latarray[20] = "51.8213";
latarray[21] = "51.8189";
latarray[22] = "51.7985";
latarray[23] = "51.8297";
latarray[24] = "51.8303";
latarray[25] = "51.8064";
latarray[26] = "51.8";
latarray[27] = "51.795;5"
latarray[28] = "51.7877;"
latarray[29] = "51.8045";
latarray[30] = "51.799";
latarray[31] = "51.8086";
latarray[32] = "51.7662";
latarray[33] = "51.7471";
latarray[34] = "51.733";
latarray[35] = "51.7661";
latarray[36] = "51.7643";
latarray[37] = "51.7549";
latarray[38] = "51.7607";
latarray[39] = "52.4761";
latarray[40] = "52.4797";
latarray[41] = "52.4837";
latarray[42] = "52.4786";
latarray[43] = "52.4939";
latarray[44] = "52.4793";
latarray[45] = "52.4799";
latarray[46] = "52.483";
latarray[47] = "52.4829";
latarray[48] = "52.4807";
latarray[49] = "52.4832";
latarray[50] = "52.4843";
latarray[51] = "52.4768";
latarray[52] = "52.4784";
latarray[53] = "52.4734";
latarray[54] = "52.4645";
latarray[55] = "52.4944";
latarray[56] = "52.501";
latarray[57] = "52.506";
latarray[58] = "52.5089";
latarray[59] = "52.496";
latarray[60] = "52.4891";
latarray[61] = "52.4993";
latarray[62] = "52.4889";
latarray[63] = "52.4932";
latarray[64] = "52.4874";
latarray[65] = "52.4777";
latarray[66] = "52.4784";
latarray[67] = "52.478";
latarray[68] = "52.469";
latarray[69] = "52.4712";
latarray[70] = "52.4622";
latarray[71] = "52.4563";
latarray[72] = "52.4498";
latarray[73] = "52.4514";
latarray[74] = "52.458";
latarray[75] = "52.4677";
latarray[76] = "52.4561";
latarray[77] = "52.4578";
latarray[78] = "52.4266";
latarray[79] = "52.4275";
latarray[80] = "52.4445";
latarray[81] = "52.44";
latarray[82] = "52.4131;"
latarray[83] = "52.4085";
latarray[84] = "52.4218";
latarray[85] = "52.432";
latarray[86] = "52.4728";
latarray[87] = "52.4643";
latarray[88] = "52.4682";
latarray[89] = "52.4807";
latarray[90] = "52.4734";
latarray[91] = "52.4747";
latarray[92] = "52.4743";
latarray[93] = "52.4538";
latarray[94] = "52.4701";
latarray[95] = "52.4607";
latarray[96] = "52.4918";
latarray[97] = "52.4949";
latarray[98] = "52.4895";
latarray[99] = "52.4881";

var hblatarray = [];
hblatarray[0] = '51.700329';
hblatarray[1] = '51.700179';
hblatarray[2] = '51.70006';
hblatarray[3] = '51.69995';
hblatarray[4] = '51.69984';
hblatarray[5] = '51.699701';
hblatarray[6] = '51.699572';
hblatarray[7] = '51.699422';
hblatarray[8] = '51.699299';
hblatarray[9] = '51.699186';
hblatarray[10] = '51.699027';
hblatarray[11] = '51.698907';
hblatarray[12] = '51.698784';
hblatarray[13] = '51.698671';
hblatarray[14] = '51.698528';
hblatarray[15] = '51.698378';
hblatarray[16] = '51.698398';
hblatarray[17] = '51.698565';
hblatarray[18] = '51.698708';
hblatarray[19] = '51.698874';
hblatarray[20] = '51.699';
hblatarray[21] = '51.699136';
hblatarray[22] = '51.699202';
hblatarray[23] = '51.699296';
hblatarray[24] = '51.699382';
hblatarray[25] = '51.699472';
hblatarray[26] = '51.699575';
hblatarray[27] = '51.699678';
hblatarray[28] = '51.699758';
hblatarray[29] = '51.699813';
hblatarray[30] = '51.699902';
hblatarray[31] = '51.699972';
hblatarray[32] = '51.700026';
hblatarray[33] = '51.700082';
hblatarray[34] = '51.700124';
hblatarray[35] = '51.700168';
hblatarray[36] = '51.70021';
hblatarray[37] = '51.700252';
hblatarray[38] = '51.700287';
hblatarray[39] = '51.700338';
hblatarray[40] = '51.700468';
hblatarray[41] = '51.700582';
hblatarray[42] = '51.700705';
hblatarray[43] = '51.700825';
hblatarray[44] = '51.700932';
hblatarray[45] = '51.701071';
hblatarray[46] = '51.701166';
hblatarray[47] = '51.701287';
hblatarray[48] = '51.701376';
hblatarray[49] = '51.701552';
hblatarray[50] = '51.701661';
hblatarray[51] = '51.701818';
hblatarray[52] = '51.701916';
hblatarray[53] = '51.70205';
hblatarray[54] = '51.702198';
hblatarray[55] = '51.702471';
hblatarray[56] = '51.702634';
hblatarray[57] = '51.702803';
hblatarray[58] = '51.702989';
hblatarray[59] = '51.703112';
hblatarray[60] = '51.703345';
hblatarray[61] = '51.703561';
hblatarray[62] = '51.703614';
hblatarray[63] = '51.703687';
hblatarray[64] = '51.703774';
hblatarray[65] = '51.703926';
hblatarray[66] = '51.704035';
hblatarray[67] = '51.704161';
hblatarray[68] = '51.70429';
hblatarray[69] = '51.704423';
hblatarray[70] = '51.704549';
hblatarray[71] = '51.704656';
hblatarray[72] = '51.704759';
hblatarray[73] = '51.704835';
hblatarray[74] = '51.704712';
hblatarray[75] = '51.704622';
hblatarray[76] = '51.704542';
hblatarray[77] = '51.704366';
hblatarray[78] = '51.70425';
hblatarray[79] = '51.704215';
hblatarray[80] = '51.704206';
hblatarray[81] = '51.70427';
hblatarray[82] = '51.704353';
hblatarray[83] = '51.704446';
hblatarray[84] = '51.70456';
hblatarray[85] = '51.704612';
hblatarray[86] = '51.704688';
hblatarray[87] = '51.704749';
hblatarray[88] = '51.704812';
hblatarray[89] = '51.704883';
hblatarray[90] = '51.704974';
hblatarray[91] = '51.705071';
hblatarray[92] = '51.705176';
hblatarray[93] = '51.705318';
hblatarray[94] = '51.705437';
hblatarray[95] = '51.70555';
hblatarray[96] = '51.70565';
hblatarray[97] = '51.705771';
hblatarray[98] = '51.705852';
hblatarray[99] = '51.705925';
hblatarray[100] = '51.70599';
hblatarray[101] = '51.706085';
hblatarray[102] = '51.706197';
hblatarray[103] = '51.706303';
hblatarray[104] = '51.706496';
hblatarray[105] = '51.706737';
hblatarray[106] = '51.706932';
hblatarray[107] = '51.707166';
hblatarray[108] = '51.707459';
hblatarray[109] = '51.707745';
hblatarray[110] = '51.708031';
hblatarray[111] = '51.70841';
hblatarray[112] = '51.708709';
hblatarray[113] = '51.709068';
hblatarray[114] = '51.70944';
hblatarray[115] = '51.709812';
hblatarray[116] = '51.710085';
hblatarray[117] = '51.710351';
hblatarray[118] = '51.710537';
hblatarray[119] = '51.710603';
hblatarray[120] = '51.710663';
hblatarray[121] = '51.710736';
hblatarray[122] = '51.710909';
hblatarray[123] = '51.711102';
hblatarray[124] = '51.711341';
hblatarray[125] = '51.711567';
hblatarray[126] = '51.711866';
hblatarray[127] = '51.711833';
hblatarray[128] = '51.711534';
hblatarray[129] = '51.711321';
hblatarray[130] = '51.711095';
hblatarray[131] = '51.710823';
hblatarray[132] = '51.710477';
hblatarray[133] = '51.710289';
hblatarray[134] = '51.710375';
hblatarray[135] = '51.710701';
hblatarray[136] = '51.711053';
hblatarray[137] = '51.711479';
hblatarray[138] = '51.711864';
hblatarray[139] = '51.712243';
hblatarray[140] = '51.712735';
hblatarray[141] = '51.713141';
hblatarray[142] = '51.713546';
hblatarray[143] = '51.714005';
hblatarray[144] = '51.71441';
hblatarray[145] = '51.714909';
hblatarray[146] = '51.715447';
hblatarray[147] = '51.716281';
hblatarray[148] = '51.716633';
hblatarray[149] = '51.716982';
hblatarray[150] = '51.717331';
hblatarray[151] = '51.717677';
hblatarray[152] = '51.718016';
hblatarray[153] = '51.718362';
hblatarray[154] = '51.718721';
hblatarray[155] = '51.719073';
hblatarray[156] = '51.719412';
hblatarray[157] = '51.719754';
hblatarray[158] = '51.720128';
hblatarray[159] = '51.720453';
hblatarray[160] = '51.720736';
hblatarray[161] = '51.721121';
hblatarray[162] = '51.721212';
hblatarray[163] = '51.721334';
hblatarray[164] = '51.721475';
hblatarray[165] = '51.721494';
hblatarray[166] = '51.72163';
hblatarray[167] = '51.721856';
hblatarray[168] = '51.722152';
hblatarray[169] = '51.722455';
hblatarray[170] = '51.722721';
hblatarray[171] = '51.723047';
hblatarray[172] = '51.723173';
hblatarray[173] = '51.723213';
hblatarray[174] = '51.723611';
hblatarray[175] = '51.72399';
hblatarray[176] = '51.723698';
hblatarray[177] = '51.723292';
hblatarray[178] = '51.722827';
hblatarray[179] = '51.722402';
hblatarray[180] = '51.722016';
hblatarray[181] = '51.722136';
hblatarray[182] = '51.722449';
hblatarray[183] = '51.722702';
hblatarray[184] = '51.722968';
hblatarray[185] = '51.723154';
hblatarray[186] = '51.72324';
hblatarray[187] = '51.723227';
hblatarray[188] = '51.72332';
hblatarray[189] = '51.72338';
hblatarray[190] = '51.723532';
hblatarray[191] = '51.723571';
hblatarray[192] = '51.723597';
hblatarray[193] = '51.723624';
hblatarray[194] = '51.723617';
hblatarray[195] = '51.723597';
hblatarray[196] = '51.72373';
hblatarray[197] = '51.723943';
hblatarray[198] = '51.724149';
hblatarray[199] = '51.724388';
hblatarray[200] = '51.700329';
hblatarray[201] = '51.700179';
hblatarray[202] = '51.70006';
hblatarray[203] = '51.69995';
hblatarray[204] = '51.69984';
hblatarray[205] = '51.699701';
hblatarray[206] = '51.699572';
hblatarray[207] = '51.699422';
hblatarray[208] = '51.699299';
hblatarray[209] = '51.699186';
hblatarray[210] = '51.699027';
hblatarray[211] = '51.698907';
hblatarray[212] = '51.698784';
hblatarray[213] = '51.698671';
hblatarray[214] = '51.698528';
hblatarray[215] = '51.698378';
hblatarray[216] = '51.698398';
hblatarray[217] = '51.698565';
hblatarray[218] = '51.698708';
hblatarray[219] = '51.698874';
hblatarray[220] = '51.699';
hblatarray[221] = '51.699136';
hblatarray[222] = '51.699202';
hblatarray[223] = '51.699296';
hblatarray[224] = '51.699382';
hblatarray[225] = '51.699472';
hblatarray[226] = '51.699575';
hblatarray[227] = '51.699678';
hblatarray[228] = '51.699758';
hblatarray[229] = '51.699813';
hblatarray[230] = '51.699902';
hblatarray[231] = '51.699972';
hblatarray[232] = '51.700026';
hblatarray[233] = '51.700082';
hblatarray[234] = '51.700124';
hblatarray[235] = '51.700168';
hblatarray[236] = '51.70021';
hblatarray[237] = '51.700252';
hblatarray[238] = '51.700287';
hblatarray[239] = '51.700338';
hblatarray[240] = '51.700468';
hblatarray[241] = '51.700582';
hblatarray[242] = '51.700705';
hblatarray[243] = '51.700825';
hblatarray[244] = '51.700932';
hblatarray[245] = '51.701071';
hblatarray[246] = '51.701166';
hblatarray[247] = '51.701287';
hblatarray[248] = '51.701376';
hblatarray[249] = '51.701552';
hblatarray[250] = '51.701661';
hblatarray[251] = '51.701818';
hblatarray[252] = '51.701916';
hblatarray[253] = '51.70205';
hblatarray[254] = '51.702198';
hblatarray[255] = '51.702471';
hblatarray[256] = '51.702634';
hblatarray[257] = '51.702803';
hblatarray[258] = '51.702989';
hblatarray[259] = '51.703112';
hblatarray[260] = '51.703345';
hblatarray[261] = '51.703561';
hblatarray[262] = '51.703614';
hblatarray[263] = '51.703687';
hblatarray[264] = '51.703774';
hblatarray[265] = '51.703926';
hblatarray[266] = '51.704035';
hblatarray[267] = '51.704161';
hblatarray[268] = '51.70429';
hblatarray[269] = '51.704423';
hblatarray[270] = '51.704549';
hblatarray[271] = '51.704656';
hblatarray[272] = '51.704759';
hblatarray[273] = '51.704835';
hblatarray[274] = '51.704712';
hblatarray[275] = '51.704622';
hblatarray[276] = '51.704542';
hblatarray[277] = '51.704366';
hblatarray[278] = '51.70425';
hblatarray[279] = '51.704215';
hblatarray[280] = '51.704206';
hblatarray[281] = '51.70427';
hblatarray[282] = '51.704353';
hblatarray[283] = '51.704446';
hblatarray[284] = '51.70456';
hblatarray[285] = '51.704612';
hblatarray[286] = '51.704688';
hblatarray[287] = '51.704749';
hblatarray[288] = '51.704812';
hblatarray[289] = '51.704883';
hblatarray[290] = '51.704974';
hblatarray[291] = '51.705071';
hblatarray[292] = '51.705176';
hblatarray[293] = '51.705318';
hblatarray[294] = '51.705437';
hblatarray[295] = '51.70555';
hblatarray[296] = '51.70565';
hblatarray[297] = '51.705771';
hblatarray[298] = '51.705852';
hblatarray[299] = '51.705925';
hblatarray[300] = '51.700329';
hblatarray[301] = '51.700179';
hblatarray[302] = '51.70006';
hblatarray[303] = '51.69995';
hblatarray[304] = '51.69984';
hblatarray[305] = '51.699701';
hblatarray[306] = '51.699572';
hblatarray[307] = '51.699422';
hblatarray[308] = '51.699299';
hblatarray[309] = '51.699186';
hblatarray[310] = '51.699027';
hblatarray[311] = '51.698907';
hblatarray[312] = '51.698784';
hblatarray[313] = '51.698671';
hblatarray[314] = '51.698528';
hblatarray[315] = '51.698378';
hblatarray[316] = '51.698398';
hblatarray[317] = '51.698565';
hblatarray[318] = '51.698708';
hblatarray[319] = '51.698874';
hblatarray[320] = '51.699';
hblatarray[321] = '51.699136';
hblatarray[322] = '51.699202';
hblatarray[323] = '51.699296';
hblatarray[324] = '51.699382';
hblatarray[325] = '51.699472';
hblatarray[326] = '51.699575';
hblatarray[327] = '51.699678';
hblatarray[328] = '51.699758';
hblatarray[329] = '51.699813';
hblatarray[330] = '51.699902';
hblatarray[331] = '51.699972';
hblatarray[332] = '51.700026';
hblatarray[333] = '51.700082';
hblatarray[334] = '51.700124';
hblatarray[335] = '51.700168';
hblatarray[336] = '51.70021';
hblatarray[337] = '51.700252';
hblatarray[338] = '51.700287';
hblatarray[339] = '51.700338';
hblatarray[340] = '51.700468';
hblatarray[341] = '51.700582';
hblatarray[342] = '51.700705';
hblatarray[343] = '51.700825';
hblatarray[344] = '51.700932';
hblatarray[345] = '51.701071';
hblatarray[346] = '51.701166';
hblatarray[347] = '51.701287';
hblatarray[348] = '51.701376';
hblatarray[349] = '51.701552';
hblatarray[350] = '51.701661';
hblatarray[351] = '51.701818';
hblatarray[352] = '51.701916';
hblatarray[353] = '51.70205';
hblatarray[354] = '51.702198';
hblatarray[355] = '51.702471';
hblatarray[356] = '51.702634';
hblatarray[357] = '51.702803';
hblatarray[358] = '51.702989';
hblatarray[359] = '51.703112';
hblatarray[360] = '51.703345';
hblatarray[361] = '51.703561';
hblatarray[362] = '51.703614';
hblatarray[363] = '51.703687';
hblatarray[364] = '51.703774';
hblatarray[365] = '51.703926';
hblatarray[366] = '51.704035';
hblatarray[367] = '51.704161';
hblatarray[368] = '51.70429';
hblatarray[369] = '51.704423';
hblatarray[370] = '51.704549';
hblatarray[371] = '51.704656';
hblatarray[372] = '51.704759';
hblatarray[373] = '51.704835';
hblatarray[374] = '51.704712';
hblatarray[375] = '51.704622';
hblatarray[376] = '51.704542';
hblatarray[377] = '51.704366';
hblatarray[378] = '51.70425';
hblatarray[379] = '51.704215';
hblatarray[380] = '51.704206';
hblatarray[381] = '51.70427';
hblatarray[382] = '51.704353';
hblatarray[383] = '51.704446';
hblatarray[384] = '51.70456';
hblatarray[385] = '51.704612';
hblatarray[386] = '51.704688';
hblatarray[387] = '51.704749';
hblatarray[388] = '51.704812';
hblatarray[389] = '51.704883';
hblatarray[390] = '51.704974';
hblatarray[391] = '51.705071';
hblatarray[392] = '51.705176';
hblatarray[393] = '51.705318';
hblatarray[394] = '51.705437';
hblatarray[395] = '51.70555';
hblatarray[396] = '51.70565';
hblatarray[397] = '51.705771';
hblatarray[398] = '51.705852';
hblatarray[399] = '51.705925';
hblatarray[400] = '51.700329';
hblatarray[401] = '51.700179';
hblatarray[402] = '51.70006';
hblatarray[403] = '51.69995';
hblatarray[404] = '51.69984';
hblatarray[405] = '51.699701';
hblatarray[406] = '51.699572';
hblatarray[407] = '51.699422';
hblatarray[408] = '51.699299';
hblatarray[409] = '51.699186';
hblatarray[410] = '51.699027';
hblatarray[411] = '51.698907';
hblatarray[412] = '51.698784';
hblatarray[413] = '51.698671';
hblatarray[414] = '51.698528';
hblatarray[415] = '51.698378';
hblatarray[416] = '51.698398';
hblatarray[417] = '51.698565';
hblatarray[418] = '51.698708';
hblatarray[419] = '51.698874';
hblatarray[420] = '51.699';
hblatarray[421] = '51.699136';
hblatarray[422] = '51.699202';
hblatarray[423] = '51.699296';
hblatarray[424] = '51.699382';
hblatarray[425] = '51.699472';
hblatarray[426] = '51.699575';
hblatarray[427] = '51.699678';
hblatarray[428] = '51.699758';
hblatarray[429] = '51.699813';
hblatarray[430] = '51.699902';
hblatarray[431] = '51.699972';
hblatarray[432] = '51.700026';
hblatarray[433] = '51.700082';
hblatarray[434] = '51.700124';
hblatarray[435] = '51.700168';
hblatarray[436] = '51.70021';
hblatarray[437] = '51.700252';
hblatarray[438] = '51.700287';
hblatarray[439] = '51.700338';
hblatarray[440] = '51.700468';
hblatarray[441] = '51.700582';
hblatarray[442] = '51.700705';
hblatarray[443] = '51.700825';
hblatarray[444] = '51.700932';
hblatarray[445] = '51.701071';
hblatarray[446] = '51.701166';
hblatarray[447] = '51.701287';
hblatarray[448] = '51.701376';
hblatarray[449] = '51.701552';
hblatarray[450] = '51.701661';
hblatarray[451] = '51.701818';
hblatarray[452] = '51.701916';
hblatarray[453] = '51.70205';
hblatarray[454] = '51.702198';
hblatarray[455] = '51.702471';
hblatarray[456] = '51.702634';
hblatarray[457] = '51.702803';
hblatarray[458] = '51.702989';
hblatarray[459] = '51.703112';
hblatarray[460] = '51.703345';
hblatarray[461] = '51.703561';
hblatarray[462] = '51.703614';
hblatarray[463] = '51.703687';
hblatarray[464] = '51.703774';
hblatarray[465] = '51.703926';
hblatarray[466] = '51.704035';
hblatarray[467] = '51.704161';
hblatarray[468] = '51.70429';
hblatarray[469] = '51.704423';
hblatarray[470] = '51.704549';
hblatarray[471] = '51.704656';
hblatarray[472] = '51.704759';
hblatarray[473] = '51.704835';
hblatarray[474] = '51.704712';
hblatarray[475] = '51.704622';
hblatarray[476] = '51.704542';
hblatarray[477] = '51.704366';
hblatarray[478] = '51.70425';
hblatarray[479] = '51.704215';
hblatarray[480] = '51.704206';
hblatarray[481] = '51.70427';
hblatarray[482] = '51.704353';
hblatarray[483] = '51.704446';
hblatarray[484] = '51.70456';
hblatarray[485] = '51.704612';
hblatarray[486] = '51.704688';
hblatarray[487] = '51.704749';
hblatarray[488] = '51.704812';
hblatarray[489] = '51.704883';
hblatarray[490] = '51.704974';
hblatarray[491] = '51.705071';
hblatarray[492] = '51.705176';
hblatarray[493] = '51.705318';
hblatarray[494] = '51.705437';
hblatarray[495] = '51.70555';
hblatarray[496] = '51.70565';
hblatarray[497] = '51.705771';
hblatarray[498] = '51.705852';
hblatarray[499] = '51.705925';

var hblangarray = [];
hblangarray[0] = '0.10999';
hblangarray[1] = '0.109679';
hblangarray[2] = '0.109395';
hblangarray[3] = '0.109159';
hblangarray[4] = '0.108944';
hblangarray[5] = '0.109037';
hblangarray[6] = '0.109133';
hblangarray[7] = '0.109241';
hblangarray[8] = '0.109326';
hblangarray[9] = '0.109402';
hblangarray[10] = '0.10953';
hblangarray[11] = '0.109681';
hblangarray[12] = '0.109868';
hblangarray[13] = '0.110024';
hblangarray[14] = '0.110233';
hblangarray[15] = '0.110474';
hblangarray[16] = '0.110662';
hblangarray[17] = '0.110855';
hblangarray[18] = '0.111022';
hblangarray[19] = '0.111199';
hblangarray[20] = '0.111338';
hblangarray[21] = '0.111504';
hblangarray[22] = '0.111571';
hblangarray[23] = '0.111405';
hblangarray[24] = '0.111249';
hblangarray[25] = '0.111131';
hblangarray[26] = '0.110938';
hblangarray[27] = '0.110825';
hblangarray[28] = '0.110849';
hblangarray[29] = '0.110894';
hblangarray[30] = '0.110778';
hblangarray[31] = '0.110686';
hblangarray[32] = '0.110612';
hblangarray[33] = '0.110534';
hblangarray[34] = '0.110485';
hblangarray[35] = '0.110423';
hblangarray[36] = '0.110363';
hblangarray[37] = '0.110308';
hblangarray[38] = '0.110255';
hblangarray[39] = '0.110192';
hblangarray[40] = '0.110058';
hblangarray[41] = '0.109995';
hblangarray[42] = '0.1099';
hblangarray[43] = '0.109751';
hblangarray[44] = '0.109621';
hblangarray[45] = '0.109711';
hblangarray[46] = '0.109851';
hblangarray[47] = '0.110004';
hblangarray[48] = '0.110126';
hblangarray[49] = '0.110266';
hblangarray[50] = '0.110266';
hblangarray[51] = '0.110288';
hblangarray[52] = '0.110252';
hblangarray[53] = '0.11018';
hblangarray[54] = '0.110139';
hblangarray[55] = '0.110134';
hblangarray[56] = '0.110134';
hblangarray[57] = '0.110177';
hblangarray[58] = '0.110193';
hblangarray[59] = '0.110198';
hblangarray[60] = '0.110241';
hblangarray[61] = '0.110262';
hblangarray[62] = '0.110053';
hblangarray[63] = '0.109747';
hblangarray[64] = '0.109495';
hblangarray[65] = '0.109275';
hblangarray[66] = '0.109198';
hblangarray[67] = '0.109243';
hblangarray[68] = '0.109299';
hblangarray[69] = '0.109396';
hblangarray[70] = '0.109524';
hblangarray[71] = '0.109653';
hblangarray[72] = '0.109825';
hblangarray[73] = '0.10997';
hblangarray[74] = '0.110163';
hblangarray[75] = '0.110378';
hblangarray[76] = '0.110576';
hblangarray[77] = '0.110646';
hblangarray[78] = '0.110906';
hblangarray[79] = '0.111142';
hblangarray[80] = '0.111357';
hblangarray[81] = '0.111558';
hblangarray[82] = '0.111657';
hblangarray[83] = '0.111708';
hblangarray[84] = '0.111687';
hblangarray[85] = '0.111842';
hblangarray[86] = '0.112048';
hblangarray[87] = '0.112198';
hblangarray[88] = '0.112319';
hblangarray[89] = '0.112456';
hblangarray[90] = '0.112619';
hblangarray[91] = '0.112749';
hblangarray[92] = '0.112887';
hblangarray[93] = '0.113021';
hblangarray[94] = '0.113107';
hblangarray[95] = '0.113151';
hblangarray[96] = '0.113209';
hblangarray[97] = '0.113327';
hblangarray[98] = '0.113391';
hblangarray[99] = '0.113531';
hblangarray[100] = '0.1137';
hblangarray[101] = '0.113592';
hblangarray[102] = '0.113425';
hblangarray[103] = '0.113277';
hblangarray[104] = '0.11294';
hblangarray[105] = '0.112525';
hblangarray[106] = '0.112146';
hblangarray[107] = '0.111779';
hblangarray[108] = '0.111307';
hblangarray[109] = '0.110878';
hblangarray[110] = '0.110374';
hblangarray[111] = '0.10973';
hblangarray[112] = '0.109258';
hblangarray[113] = '0.108861';
hblangarray[114] = '0.108496';
hblangarray[115] = '0.107906';
hblangarray[116] = '0.107219';
hblangarray[117] = '0.106468';
hblangarray[118] = '0.10575';
hblangarray[119] = '0.104816';
hblangarray[120] = '0.103926';
hblangarray[121] = '0.102896';
hblangarray[122] = '0.101994';
hblangarray[123] = '0.101146';
hblangarray[124] = '0.100213';
hblangarray[125] = '0.099516';
hblangarray[126] = '0.09884';
hblangarray[127] = '0.097992';
hblangarray[128] = '0.097348';
hblangarray[129] = '0.096715';
hblangarray[130] = '0.095964';
hblangarray[131] = '0.095406';
hblangarray[132] = '0.094977';
hblangarray[133] = '0.09458';
hblangarray[134] = '0.093947';
hblangarray[135] = '0.093592';
hblangarray[136] = '0.093292';
hblangarray[137] = '0.09297';
hblangarray[138] = '0.092648';
hblangarray[139] = '0.092348';
hblangarray[140] = '0.091983';
hblangarray[141] = '0.091758';
hblangarray[142] = '0.091511';
hblangarray[143] = '0.091264';
hblangarray[144] = '0.091007';
hblangarray[145] = '0.090749';
hblangarray[146] = '0.090685';
hblangarray[147] = '0.090733';
hblangarray[148] = '0.090862';
hblangarray[149] = '0.09098';
hblangarray[150] = '0.090985';
hblangarray[151] = '0.091077';
hblangarray[152] = '0.091243';
hblangarray[153] = '0.091367';
hblangarray[154] = '0.091334';
hblangarray[155] = '0.091238';
hblangarray[156] = '0.091109';
hblangarray[157] = '0.090975';
hblangarray[158] = '0.090846';
hblangarray[159] = '0.090712';
hblangarray[160] = '0.090589';
hblangarray[161] = '0.090498';
hblangarray[162] = '0.091102';
hblangarray[163] = '0.0918';
hblangarray[164] = '0.092506';
hblangarray[165] = '0.093143';
hblangarray[166] = '0.093856';
hblangarray[167] = '0.094554';
hblangarray[168] = '0.09532';
hblangarray[169] = '0.096131';
hblangarray[170] = '0.096732';
hblangarray[171] = '0.09729';
hblangarray[172] = '0.098116';
hblangarray[173] = '0.098846';
hblangarray[174] = '0.099425';
hblangarray[175] = '0.100058';
hblangarray[176] = '0.100541';
hblangarray[177] = '0.100927';
hblangarray[178] = '0.101313';
hblangarray[179] = '0.101667';
hblangarray[180] = '0.102064';
hblangarray[181] = '0.102912';
hblangarray[182] = '0.103749';
hblangarray[183] = '0.104468';
hblangarray[184] = '0.105208';
hblangarray[185] = '0.106377';
hblangarray[186] = '0.107161';
hblangarray[187] = '0.107955';
hblangarray[188] = '0.108609';
hblangarray[189] = '0.109221';
hblangarray[190] = '0.110057';
hblangarray[191] = '0.110704';
hblangarray[192] = '0.111381';
hblangarray[193] = '0.112519';
hblangarray[194] = '0.113635';
hblangarray[195] = '0.114901';
hblangarray[196] = '0.116006';
hblangarray[197] = '0.116896';
hblangarray[198] = '0.11769';
hblangarray[199] = '0.1137';
hblangarray[200] = '0.10999';
hblangarray[201] = '0.109679';
hblangarray[202] = '0.109395';
hblangarray[203] = '0.109159';
hblangarray[204] = '0.108944';
hblangarray[205] = '0.109037';
hblangarray[206] = '0.109133';
hblangarray[207] = '0.109241';
hblangarray[208] = '0.109326';
hblangarray[209] = '0.109402';
hblangarray[210] = '0.10953';
hblangarray[211] = '0.109681';
hblangarray[212] = '0.109868';
hblangarray[213] = '0.110024';
hblangarray[214] = '0.110233';
hblangarray[215] = '0.110474';
hblangarray[216] = '0.110662';
hblangarray[217] = '0.110855';
hblangarray[218] = '0.111022';
hblangarray[219] = '0.111199';
hblangarray[220] = '0.111338';
hblangarray[221] = '0.111504';
hblangarray[222] = '0.111571';
hblangarray[223] = '0.111405';
hblangarray[224] = '0.111249';
hblangarray[225] = '0.111131';
hblangarray[226] = '0.110938';
hblangarray[227] = '0.110825';
hblangarray[228] = '0.110849';
hblangarray[229] = '0.110894';
hblangarray[230] = '0.110778';
hblangarray[231] = '0.110686';
hblangarray[232] = '0.110612';
hblangarray[233] = '0.110534';
hblangarray[234] = '0.110485';
hblangarray[235] = '0.110423';
hblangarray[236] = '0.110363';
hblangarray[237] = '0.110308';
hblangarray[238] = '0.110255';
hblangarray[239] = '0.110192';
hblangarray[240] = '0.110058';
hblangarray[241] = '0.109995';
hblangarray[242] = '0.1099';
hblangarray[243] = '0.109751';
hblangarray[244] = '0.109621';
hblangarray[245] = '0.109711';
hblangarray[246] = '0.109851';
hblangarray[247] = '0.110004';
hblangarray[248] = '0.110126';
hblangarray[249] = '0.110266';
hblangarray[250] = '0.110266';
hblangarray[251] = '0.110288';
hblangarray[252] = '0.110252';
hblangarray[253] = '0.11018';
hblangarray[254] = '0.110139';
hblangarray[255] = '0.110134';
hblangarray[256] = '0.110134';
hblangarray[257] = '0.110177';
hblangarray[258] = '0.110193';
hblangarray[259] = '0.110198';
hblangarray[260] = '0.110241';
hblangarray[261] = '0.110262';
hblangarray[262] = '0.110053';
hblangarray[263] = '0.109747';
hblangarray[264] = '0.109495';
hblangarray[265] = '0.109275';
hblangarray[266] = '0.109198';
hblangarray[267] = '0.109243';
hblangarray[268] = '0.109299';
hblangarray[269] = '0.109396';
hblangarray[270] = '0.109524';
hblangarray[271] = '0.109653';
hblangarray[272] = '0.109825';
hblangarray[273] = '0.10997';
hblangarray[274] = '0.110163';
hblangarray[275] = '0.110378';
hblangarray[276] = '0.110576';
hblangarray[277] = '0.110646';
hblangarray[278] = '0.110906';
hblangarray[279] = '0.111142';
hblangarray[280] = '0.111357';
hblangarray[281] = '0.111558';
hblangarray[282] = '0.111657';
hblangarray[283] = '0.111708';
hblangarray[284] = '0.111687';
hblangarray[285] = '0.111842';
hblangarray[286] = '0.112048';
hblangarray[287] = '0.112198';
hblangarray[288] = '0.112319';
hblangarray[289] = '0.112456';
hblangarray[290] = '0.112619';
hblangarray[291] = '0.112749';
hblangarray[292] = '0.112887';
hblangarray[293] = '0.113021';
hblangarray[294] = '0.113107';
hblangarray[295] = '0.113151';
hblangarray[296] = '0.113209';
hblangarray[297] = '0.113327';
hblangarray[298] = '0.113391';
hblangarray[299] = '0.113531';
hblangarray[300] = '0.10999';
hblangarray[301] = '0.109679';
hblangarray[302] = '0.109395';
hblangarray[303] = '0.109159';
hblangarray[304] = '0.108944';
hblangarray[305] = '0.109037';
hblangarray[306] = '0.109133';
hblangarray[307] = '0.109241';
hblangarray[308] = '0.109326';
hblangarray[309] = '0.109402';
hblangarray[310] = '0.10953';
hblangarray[311] = '0.109681';
hblangarray[312] = '0.109868';
hblangarray[313] = '0.110024';
hblangarray[314] = '0.110233';
hblangarray[315] = '0.110474';
hblangarray[316] = '0.110662';
hblangarray[317] = '0.110855';
hblangarray[318] = '0.111022';
hblangarray[319] = '0.111199';
hblangarray[320] = '0.111338';
hblangarray[321] = '0.111504';
hblangarray[322] = '0.111571';
hblangarray[323] = '0.111405';
hblangarray[324] = '0.111249';
hblangarray[325] = '0.111131';
hblangarray[326] = '0.110938';
hblangarray[327] = '0.110825';
hblangarray[328] = '0.110849';
hblangarray[329] = '0.110894';
hblangarray[330] = '0.110778';
hblangarray[331] = '0.110686';
hblangarray[332] = '0.110612';
hblangarray[333] = '0.110534';
hblangarray[334] = '0.110485';
hblangarray[335] = '0.110423';
hblangarray[336] = '0.110363';
hblangarray[337] = '0.110308';
hblangarray[338] = '0.110255';
hblangarray[339] = '0.110192';
hblangarray[340] = '0.110058';
hblangarray[341] = '0.109995';
hblangarray[342] = '0.1099';
hblangarray[343] = '0.109751';
hblangarray[344] = '0.109621';
hblangarray[345] = '0.109711';
hblangarray[346] = '0.109851';
hblangarray[347] = '0.110004';
hblangarray[348] = '0.110126';
hblangarray[349] = '0.110266';
hblangarray[350] = '0.110266';
hblangarray[351] = '0.110288';
hblangarray[352] = '0.110252';
hblangarray[353] = '0.11018';
hblangarray[354] = '0.110139';
hblangarray[355] = '0.110134';
hblangarray[356] = '0.110134';
hblangarray[357] = '0.110177';
hblangarray[358] = '0.110193';
hblangarray[359] = '0.110198';
hblangarray[360] = '0.110241';
hblangarray[361] = '0.110262';
hblangarray[362] = '0.110053';
hblangarray[363] = '0.109747';
hblangarray[364] = '0.109495';
hblangarray[365] = '0.109275';
hblangarray[366] = '0.109198';
hblangarray[367] = '0.109243';
hblangarray[368] = '0.109299';
hblangarray[369] = '0.109396';
hblangarray[370] = '0.109524';
hblangarray[371] = '0.109653';
hblangarray[372] = '0.109825';
hblangarray[373] = '0.10997';
hblangarray[374] = '0.110163';
hblangarray[375] = '0.110378';
hblangarray[376] = '0.110576';
hblangarray[377] = '0.110646';
hblangarray[378] = '0.110906';
hblangarray[379] = '0.111142';
hblangarray[380] = '0.111357';
hblangarray[381] = '0.111558';
hblangarray[382] = '0.111657';
hblangarray[383] = '0.111708';
hblangarray[384] = '0.111687';
hblangarray[385] = '0.111842';
hblangarray[386] = '0.112048';
hblangarray[387] = '0.112198';
hblangarray[388] = '0.112319';
hblangarray[389] = '0.112456';
hblangarray[390] = '0.112619';
hblangarray[391] = '0.112749';
hblangarray[392] = '0.112887';
hblangarray[393] = '0.113021';
hblangarray[394] = '0.113107';
hblangarray[395] = '0.113151';
hblangarray[396] = '0.113209';
hblangarray[397] = '0.113327';
hblangarray[398] = '0.113391';
hblangarray[399] = '0.113531';
hblangarray[400] = '0.10999';
hblangarray[401] = '0.109679';
hblangarray[402] = '0.109395';
hblangarray[403] = '0.109159';
hblangarray[404] = '0.108944';
hblangarray[405] = '0.109037';
hblangarray[406] = '0.109133';
hblangarray[407] = '0.109241';
hblangarray[408] = '0.109326';
hblangarray[409] = '0.109402';
hblangarray[410] = '0.10953';
hblangarray[411] = '0.109681';
hblangarray[412] = '0.109868';
hblangarray[413] = '0.110024';
hblangarray[414] = '0.110233';
hblangarray[415] = '0.110474';
hblangarray[416] = '0.110662';
hblangarray[417] = '0.110855';
hblangarray[418] = '0.111022';
hblangarray[419] = '0.111199';
hblangarray[420] = '0.111338';
hblangarray[421] = '0.111504';
hblangarray[422] = '0.111571';
hblangarray[423] = '0.111405';
hblangarray[424] = '0.111249';
hblangarray[425] = '0.111131';
hblangarray[426] = '0.110938';
hblangarray[427] = '0.110825';
hblangarray[428] = '0.110849';
hblangarray[429] = '0.110894';
hblangarray[430] = '0.110778';
hblangarray[431] = '0.110686';
hblangarray[432] = '0.110612';
hblangarray[433] = '0.110534';
hblangarray[434] = '0.110485';
hblangarray[435] = '0.110423';
hblangarray[436] = '0.110363';
hblangarray[437] = '0.110308';
hblangarray[438] = '0.110255';
hblangarray[439] = '0.110192';
hblangarray[440] = '0.110058';
hblangarray[441] = '0.109995';
hblangarray[442] = '0.1099';
hblangarray[443] = '0.109751';
hblangarray[444] = '0.109621';
hblangarray[445] = '0.109711';
hblangarray[446] = '0.109851';
hblangarray[447] = '0.110004';
hblangarray[448] = '0.110126';
hblangarray[449] = '0.110266';
hblangarray[450] = '0.110266';
hblangarray[451] = '0.110288';
hblangarray[452] = '0.110252';
hblangarray[453] = '0.11018';
hblangarray[454] = '0.110139';
hblangarray[455] = '0.110134';
hblangarray[456] = '0.110134';
hblangarray[457] = '0.110177';
hblangarray[458] = '0.110193';
hblangarray[459] = '0.110198';
hblangarray[460] = '0.110241';
hblangarray[461] = '0.110262';
hblangarray[462] = '0.110053';
hblangarray[463] = '0.109747';
hblangarray[464] = '0.109495';
hblangarray[465] = '0.109275';
hblangarray[466] = '0.109198';
hblangarray[467] = '0.109243';
hblangarray[468] = '0.109299';
hblangarray[469] = '0.109396';
hblangarray[470] = '0.109524';
hblangarray[471] = '0.109653';
hblangarray[472] = '0.109825';
hblangarray[473] = '0.10997';
hblangarray[474] = '0.110163';
hblangarray[475] = '0.110378';
hblangarray[476] = '0.110576';
hblangarray[477] = '0.110646';
hblangarray[478] = '0.110906';
hblangarray[479] = '0.111142';
hblangarray[480] = '0.111357';
hblangarray[481] = '0.111558';
hblangarray[482] = '0.111657';
hblangarray[483] = '0.111708';
hblangarray[484] = '0.111687';
hblangarray[485] = '0.111842';
hblangarray[486] = '0.112048';
hblangarray[487] = '0.112198';
hblangarray[488] = '0.112319';
hblangarray[489] = '0.112456';
hblangarray[490] = '0.112619';
hblangarray[491] = '0.112749';
hblangarray[492] = '0.112887';
hblangarray[493] = '0.113021';
hblangarray[494] = '0.113107';
hblangarray[495] = '0.113151';
hblangarray[496] = '0.113209';
hblangarray[497] = '0.113327';
hblangarray[498] = '0.113391';
hblangarray[499] = '0.113531';


var hbinterval = 0;


$(document).ready(function () {

    $.session.set("timervalue", 0);
    
    addofficer();

    if ($.session.get("timervalue") >= 0) {
        Timer();
    }
    
    $('#btnAddOfficer').click(function () {
        addofficer();
        //$("#officercontainer").append($("#officertemplate").html());
    });

    $('#btnStartAutoAction').click(function () {
        var l_interval = $.trim($("#autointerval").val().replace(/[^0-9]/g, ''));
        if (l_interval.length > 0) {
            l_interval = parseInt(l_interval);
        }
        else {
            l_interval = 0;
        }

        if ((l_interval > 0) && (l_interval < 99) && (autoactioninterval == 0)) {
            updateconsole("Auto Action started with Interval:" + l_interval + " & ID:" + autoactioninterval);
            $("#autointerval").attr('disabled', 'disabled');
            $("#btnStartAutoAction").attr('disabled', 'disabled');
            autoactioninterval = setInterval(function () {
                $("#btnAction").trigger('click');
            }, l_interval);
        }
        else {
            updateconsole("Auto Action NOT started Interval:" + l_interval + " & ID:" + autoactioninterval);
        }
    });

    $('#btnStopAutoAction').click(function () {
        clearInterval(autoactioninterval);
        autoactioninterval = 0;
        $("#autointerval").val('');
        $("#autointerval").removeAttr('disabled');
        $("#btnStartAutoAction").removeAttr('disabled');
        updateconsole("Auto Action stopped externally... ");
    });

    //Button Action
    $('#btnAction').click(function () {       
        if ($.trim(actionbuttons).length > 0) {
            $("" + actionbuttons + "").trigger('click');
        }
        //$(".btnSimulate").trigger('click');
        /*
        $("#officercontainer > .officerdiv").each(function () {
                       
            if ($(this).find(".logindiv").find(".loggedofficerid").length > 0) {
                var l_officerid = $.trim($(this).find(".logindiv").find(".loggedofficerid").val().replace(/[^0-9]/g, ''));
                var l_action = $.trim($(this).find(".actiondiv").find(".actionselect option:selected").val().replace(/[^0-9]/g, ''));
                var l_actioncount = $.trim($(this).find(".actiondiv").find(".actioncount").val().replace(/[^0-9]/g, ''));
                if (l_officerid.length > 0) {
                    if (l_action.length > 0) {
                        if ((l_actioncount.length > 0) && (l_actioncount > 0)) {
                            doAction(l_officerid, l_action, l_actioncount);
                        }
                        else {
                            alert("No.of Action is empty...");
                        }
                    }
                    else {
                        alert("No action selected...");
                    }
                }
                else {
                    alert("Logged Officer ID is empty");
                }
            }
            else {
                alert("No Officer Logged in...");
            }
        });
        */
        
    });

    $(document).on('click', '.clsClose', function (evt) {

        if ($("#officercontainer > div").length > 1) {
            $(this).fadeOut('slow', function () {
                $(this).parent().parent().remove();                
                var l_officerid = $(this).parent().parent().find(".logindiv").find(".loggedofficerid").val().replace(/[^0-9]/g, '');
                Logout(l_officerid);
            });
        }
    });

    $('#btnLogout').click(function () {

        //-- Pass a function to another function with parameters 

        /*
        var testarray = new Array();
        testarray['A'] = 'aaa';
        testarray['B'] = 'bbb';
        testarray['C'] = 'ccc';

        if (testarray.hasOwnProperty('A')) {
            alert(testarray['A']);
        }
        */
        /*
        var simulatefunction = function (callback, p1) { setTimeout(function () { callback(...p1) }, 3000); };
        var x = new Array();
        var a = '"updateconsole,"' + "'test1'";
        x[0] = simulatefunction;
        var functptr = mytest;
        var paramlist = new Array();
        paramlist.push("test111");
        paramlist.push("test222");
        */
        //paramlist[1] = "test2";
        //x[1] = simulatefunction;
        //x[2] = simulatefunction;
        //console.log(paramlist);
        //x[0](functptr, paramlist);
        //x[1](a, 'test2');
        //x[2](a, 'test3');

        //x[i](actionmethod[0],)
        
        //for (var i = 0; i < 100; i++)
        //    updateconsole(Math.floor(Math.random() * 6));
                
        $("#officercontainer > .officerdiv").each(function () {
            if ($(this).find(".logindiv").find(".loggedofficerid").length > 0) {
                var l_officerid = $.trim($(this).find(".logindiv").find(".loggedofficerid").val().replace(/[^0-9]/g, ''));
                Logout(l_officerid);
            }
        });
        setTimeout(function () {
            window.location.href = "login.html";    
        }, 3000);        
        
    });

    /*
    $(document).on('click', '.btnSimulate', function (evt) {
        var l_officerid = $(this).parent().parent().parent().parent().find(".logindiv").find(".officerid").val();
        var l_selectedtext = $(this).parent().parent().parent().find(".actionselect option:selected").text();
        var l_actioncount = $(this).parent().parent().parent().find(".actioncount").val();
        $("#consolelog").append("\n" + l_officerid + "simulate started");
        scrollTextArea();
        setTimeout(testsimulate(l_officerid, l_selectedtext, l_actioncount),1000);
    });
    */

    $(document).on('click', '.btnLogin', function (evt) {

        var officerid = $(this).parent().parent().parent().parent().find(".logindiv").find(".officerid").val().replace(/[^0-9]/g, '');
        var password = $(this).parent().parent().parent().parent().find(".logindiv").find(".password").val().replace(/[^0-9]/g, '');

        if (GetToken(officerid, password) == "success") {
            updateconsole(officerid + " - Login Success");
            $(this).parent().parent().parent().parent().find(".actiondiv").find(".actionselect").removeAttr("disabled");
            $(this).parent().parent().parent().parent().find(".actiondiv").find(".actioncount").removeAttr("disabled");
            $(this).parent().parent().parent().parent().find(".actiondiv").find(".logbox").show();
            $(this).parent().parent().parent().parent().find(".logindiv").find(".officerid").attr("disabled", "disabled");
            $(this).parent().parent().parent().parent().find(".logindiv").find(".password").attr("disabled", "disabled");
            $(this).parent().parent().parent().parent().find(".logindiv").find(".btnLogin").attr("disabled", "disabled");
            $(this).parent().parent().parent().parent().find(".logindiv").find(".btnLogin").html("Logged");
            $(this).parent().parent().parent().parent().find(".logindiv").find(".btnLogin").removeClass("btnLogin");            
            $(this).parent().parent().parent().parent().find(".logindiv").find(".officerid").addClass("loggedofficerid");            
        }
        else {
            updateconsole(officerid + " - Login Failed");
        }        
    });

    $(document).on('change', '.actionselect', function (evt) {
        var l_actionval = $(this).val();
        if (l_actionval == '999') {
            $(this).parent().parent().parent().find(".hbintervalcheck").show();
        }
        else {
            $(this).parent().parent().parent().find(".hbintervalcheck").hide();
        }
    });
    
    $(document).on('click', '.clsIntervalCheck', function (evt) {
        var l_chkinterval = 0;

        $(this).parent().parent().parent().find('.clsIntervalCheck:checkbox:checked').each(function () {
            l_chkinterval = $(this).val();
        });
        if (l_chkinterval == "1") {
            $(this).parent().parent().parent().find(".actioncount").val('1');
            $(this).parent().parent().parent().find(".actioncount").attr("disabled", "disabled");
            $(this).parent().parent().parent().find(".interval").removeAttr("disabled");
            $(this).parent().parent().parent().find(".steps").removeAttr("disabled");
            $(this).parent().parent().parent().find(".interval").val('1');
            $(this).parent().parent().parent().find(".steps").val('1');
        }
        else {
            $(this).parent().parent().parent().find(".actioncount").val('1');
            $(this).parent().parent().parent().find(".interval").val('');
            $(this).parent().parent().parent().find(".steps").val('');
            $(this).parent().parent().parent().find(".actioncount").removeAttr("disabled");
            $(this).parent().parent().parent().find(".interval").attr("disabled", "disabled");
            $(this).parent().parent().parent().find(".steps").attr("disabled", "disabled");
        }

    });
    
    $('.actioncount').on('input', function () {
        $(this).val($(this).val().replace(/[^0-9]/g, ''));
    });
   
});

function GetToken(officerid, password) {
    var l_success = "Login Failed";
    $.ajax({
        type: 'POST',
        async : false,
        url: ServiceURL + 'token',       
        data: {
            "grant_type": "password",
            "username": officerid,
            "password": password
        },
        dataType: DataType,
        success: function (data, textStatus, xhr) {
            $.session.set(officerid+"timervalue", 0);
            //$.session.set('Token', data.access_token);
            //$.session.set('TokenType', data.token_type);
            $.session.set(officerid+'Token', data.access_token);
            //$.session.set('ExpiresIn', data.expires_in);
            // $.session.set('issued', data['.issued']);
            //$.session.set('expires', data['.expires']);
            $.session.set(officerid+'TokenAuthorization', data.token_type + ' ' + data.access_token);
            //Loginnew();
            l_success = "success";
            updateconsole(officerid + " - Token received...");
        },
        error: function (xhr, textStatus, errorThrown) {
            //$('#lblUNPWDWarningInfo').html("Officer ID and Password didnot match.");
        }
    });
    return l_success;
}

function Loginnew(officerid) {
    $.ajax({
        type: 'POST',
        url: ServiceURL + 'api/v1/officers/' + $.session.get('OfficerID') + '/Login',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: DataType,
        success: function (data) {
            if (!(data == '' || data == null || data == '[]')) {
                $.session.set("timervalue", 0);
                $.session.set('OfficerName', data.OfficerName);
                $.session.set('CompanyID', data.CompanyID);
                //TargetNo missing
                $.session.set('tgVal', data.TargetNo);
                $.session.set('aref', data.AutoRefresh);
                $.session.set('RoleType', data.RoleType);
                $.session.set('OfficerRole', data.OfficerRole);
                if (data.RoleType == 6) {
                    $.session.set('bc', 1);
                    $.session.set('IsVedio', 1);
                    window.top.location.href = HrUrl;// "HRView.html";
                }
                else if (data.RoleType == 5) {
                    $.session.set('bc', 1);
                    $.session.set('IsVedio', 1);
                    window.top.location.href = RedirectUrl;
                }
                else if (data.RoleType == 1) {
                    $.session.set('IsVedio', 1);
                    if (data.CompanyID == 1)
                        window.top.location = "Dashboard_MS.html";
                    else
                        window.top.location = "Dashboard_HC.html";
                }
                else if (data.RoleType == 7) {
                    if (data.CompanyID == 1)
                        window.top.location = "Dashboard_ES.html";
                    else
                        window.top.location = "Dashboard_HC.html";
                }
                else {
                    $.session.set('bc', 1);
                    $.session.set('IsVedio', 1);
                    if (data.CompanyID == 1)
                        window.top.location = "Dashboard_MS.html";
                    else
                        window.top.location = "Dashboard_HC.html";
                }
            }
            else {
                $('#lblUNPWDWarningInfo').html("Officer ID and Password did not match.");
            }
        },
        complete: function () {
        },
        error: function (xhr, textStatus, errorThrown) {
            // alert('Error');
        }
    });
}

function Timer() {
    setInterval(function () {

        $("#officercontainer > .officerdiv").each(function () {
            if ($(this).find(".logindiv").find(".loggedofficerid").length > 0) {
                var l_officerid = $.trim($(this).find(".logindiv").find(".loggedofficerid").val().replace(/[^0-9]/g, ''));
                var l_password = $.trim($(this).find(".logindiv").find(".password").val().replace(/[^0-9]/g, ''));
                var timervalue = parseInt($.session.get(l_officerid+"timervalue"));
                timervalue = timervalue + 1;
                //$("#timerid").html(timervalue);
                $.session.set(l_officerid+"timervalue", timervalue);
                if (timervalue >= 20) {
                    CheckToken(l_officerid, l_password);
                }
            }
        });
      
    }, 60000);
}

function CheckToken(a_officerid, a_password) {

    $.ajax({
        type: 'POST',
        url: ServiceURL + 'token',
        data: {
            "grant_type": "password",
            "username": a_officerid,
            "password": a_password
        },
        dataType: DataType,
        success: function (data, textStatus, xhr) {
            //console.log("Authorization : " + data.access_token);
            $.session.set(a_officerid+'Token', data.access_token);
            $.session.set(a_officerid+'TokenAuthorization', data.token_type + ' ' + data.access_token);
            $.session.set(a_officerid + 'timervalue', 0);
            //updateconsole(a_officerid + " - Token updated ...");
        },
        error: function (xhr, textStatus, errorThrown) {
            //alert('xhr  :' + JSON.stringify(xhr));
            //alert('textstatus : ' + textstatus);
            //alert(errorThrown);
        }
    });
}

//==========================================================================================================================================
// TODO : Logout function

function Logout(a_officerid) {    
    $.ajax({
        type: 'PUT',
        url: ServiceURL + 'api/v1/officers/' + a_officerid + '/Logout',
        headers: {
            "Authorization": $.session.get(a_officerid+'TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: DataType,
        beforeSend: function () {
        },
        success: function (data) {
            $.session.set(a_officerid + 'Token', null);
            $.session.set(a_officerid + 'TokenAuthorization', null);
            $.session.set(a_officerid + 'timervalue', null);
            updateconsole(a_officerid + " - Logged out ...");
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

function doAction(a_officerid, a_actionid, a_actiontxt, a_casenumber) {
    
        updateconsole(a_officerid + " - " + a_casenumber + " - " + a_actiontxt + " - success ");
    
        var d = new Date();
        var l_doorcolour = doorcolor[Math.floor(Math.random() * 15)];
        var l_housetype = housetype[Math.floor(Math.random() * 19)];
        var l_gpsindex = Math.floor(Math.random() * 100);
        var paramData = '{';
        paramData += '"CaseNumber": "' + a_casenumber + '",';
        paramData += '"Officer": "' + a_officerid.toString() + '",';
        paramData += '"ActionId": "' + a_actionid.toString() + '",';
        paramData += '"Fees": "' + "1$0" + '",';
        paramData += '"DoorColour":"' + l_doorcolour + '",';
        paramData += '"HouseType":"' + l_housetype + '",';
        paramData += '"PersonContacted":"' + 1 + '",';
        paramData += '"ContactName":"' + "Name123" + '",';
        paramData += '"Notes":"' + d.toLocaleTimeString() + " -This is a sample notes text" + '",';
        paramData += '"DateActioned":"' + new Date().toJSON().toString() + '",';
        paramData += '"GPSLatitude":"' + latarray[l_gpsindex] + '",';
        paramData += '"GPSLongitude":"' + langarray[l_gpsindex] + '",';
        paramData += '"GPSHDOP":"' + "10.00" + '",';

        if (a_actionid == '1' || a_actionid == '3') {
            paramData += '"AmountPaid":"' + "100.00" + '",';
        }
        else {
            paramData += '"AmountPaid":"' + "0.00" + '",';
        }

        if (a_actionid == '2') {
            paramData += '"ReturnCode": "' + "101" + '",';
        }
        else {
            paramData += '"ReturnCode": "' + "" + '",';
        }

        paramData += '"IsDeviated":"' + true + '"}';

        Url = ServiceURL + "api/v1/cases/columbusAction";
        
        $.ajax({
            type: 'POST',
            url: Url,
            headers: {
                "Authorization": $.session.get(a_officerid+'TokenAuthorization'),
                'Content-Type': 'application/json'
            },
            data: paramData,
            async: true,
            success: function (result) {                
            },
            error: function () {
                updateconsole(a_officerid + " - " + a_casenumber + " - " + a_actiontxt + " - failed ");
            } 
        });
 
}

function doHBAction(a_officerid, a_actiontxt, a_indexvalue) {
   
    updateconsole('index value : ' + a_indexvalue);

    var a_latvalue = hblatarray[a_indexvalue];
    var a_langvalue = hblangarray[a_indexvalue];

    var l_date = new Date().toISOString();
    var l_datearr = l_date.slice(0, 10).split("-");
    var l_timearr = l_date.split("T");
    var l_timearr = l_timearr[1].split(".");

    var a_gpstimevalue = l_datearr[2] + '-' + l_datearr[1] + '-' + l_datearr[0] + " " + l_timearr[0];

    var paramData = '{';
    paramData += '"OfficerID": "' + a_officerid + '",';
    paramData += '"MessageType": "HB",';
    paramData += '"Message": "' + a_officerid + '|' + a_latvalue + '|' + a_langvalue + '|' + '10|00|00|' + a_gpstimevalue + '|1.0.51",';
    paramData += '"Latitude": "' + a_latvalue + '",';
    paramData += '"Longitude": "' + a_langvalue + '",';
    paramData += '"Accuracy": "' + "10" + '",';
    paramData += '"CompanyID": "' + "1" + '",';
    paramData += '"DeviceTime":"' + a_gpstimevalue + '"}';

    Url = ServiceURL + "api/v1/officers/" + a_officerid + "/action";

    $.ajax({
        type: 'POST',
        url: Url,
        headers: {
            "Authorization": $.session.get(a_officerid+'TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        data: paramData,
        async: true,
        success: function (result) {
            updateconsole(a_officerid + " - " + a_actiontxt + " - " + a_gpstimevalue + " - success ");
        },
        error: function (error) {            
            updateconsole(a_officerid + " - " + a_actiontxt + " - failed - ");
        }
    });
}

function doWMAAction(a_officerid, a_wmaindex) {
    var l_wmatxt = "";

    if (a_wmaindex == 0) {
        l_wmatxt = "vrm";
        l_wmavalue = "Z";
    }
    else if (a_wmaindex == 1) {
        l_wmatxt = "postcode";
        l_wmavalue = "ip6";
    }
    else {
        l_wmatxt = "caseNumber";
        l_wmavalue = "4792329";
    }

    updateconsole(a_officerid + " - " + l_wmatxt + "-" + l_wmavalue + " - success ");

    var paramData = '{';
    paramData += '"' + l_wmatxt + '": "' + l_wmavalue + '"}';

    Url = ServiceURL + "api/v1/cases?" + l_wmatxt + "=" + l_wmavalue;

    $.ajax({
        type: 'GET',
        url: Url,
        headers: {
            "Authorization": $.session.get(a_officerid+'TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        async: true,
        success: function (result) {
        },
        error: function () {
            updateconsole(a_officerid + " - " + l_wmatxt + "-" + l_wmavalue + " - failed ");
        }
    });
}

/*
function parameterPush(paramData, paramKey, paramValue) {
    if ($.trim(paramValue) != '') {
        var myObj = JSON.parse('{"' + paramKey + '" : "' + paramValue + '"}');
        $.extend(true, paramData, myObj);
    }
    return paramData;
}
*/

function scrollTextArea() {
    var console123 = $("#consolelog");
    console123.scrollTop(console123[0].scrollHeight - console123.height());
}

function simulateorder(a_officerid, a_action, a_actiontxt, a_casenumberarray)
{
    if (a_casenumberarray.length > 0) {
        var l_casenumber = a_casenumberarray[0];
        a_casenumberarray = a_casenumberarray.slice(1);
        setTimeout(function () {
            doAction(a_officerid, a_action, a_actiontxt, l_casenumber);
        }, 2000);

        simulateorder(a_officerid, a_action, a_actiontxt, a_casenumberarray);
    }
}
                           
function simulateheartbeat(a_officerid, a_actiontxt, a_actioncount, a_interval, a_indexvalue) {
    if (a_indexvalue > hblatarray.length) {
        a_indexvalue = indexvalue - hblatarray.length;
    }
    if (a_actioncount > 0) {
       
            setTimeout(function () {
                doHBAction(a_officerid, a_actiontxt, a_indexvalue);
            }, a_interval);
        
            a_actioncount--;
            a_interval += 40000;
            a_indexvalue++;
            simulateheartbeat(a_officerid, a_actiontxt, a_actioncount, a_interval, a_indexvalue);
    }
}

function simulatewma(a_officerid, a_actioncount) {

    if (a_actioncount > 0) {
        var i = a_actioncount % 3;

        setTimeout(function () {
            doWMAAction(a_officerid, i);
        }, 2000);

        a_actioncount--;
        simulatewma(a_officerid, a_actioncount);
    }
}

function addofficer() {
    var l_officeridval = officeridarray[officercount];
    var l_passwordval = passwordarray[officercount];
    officercount++;
    if (officercount >= officeridarray.length)
        officercount = 0;

    objcnt = objcnt + 1;
    $("#officercontainer").append($("#officertemplate").html());
    $("#officercontainer .officerdiv:last-child").find(".btnSimulate").attr("id", "btnSimulate" + objcnt);
    $("#officercontainer .officerdiv:last-child").find(".logindiv").find(".officerid").val(l_officeridval);
    $("#officercontainer .officerdiv:last-child").find(".logindiv").find(".password").val(l_passwordval);
    if (objcnt > 1)
        actionbuttons += ",";
    actionbuttons += "#btnSimulate" + objcnt;

    $("#btnSimulate" + objcnt).on("click", function () {
        var l_officerid = $.trim($(this).parent().parent().parent().parent().find(".logindiv").find(".loggedofficerid").val().replace(/[^0-9]/g, ''));
        if (l_officerid.length > 0) {            
            var l_action = $.trim($(this).parent().parent().parent().find(".actionselect option:selected").val().replace(/[^0-9]/g, ''));
            var l_actiontxt = $.trim($(this).parent().parent().parent().find(".actionselect option:selected").text());
            var l_actioncount = $.trim($(this).parent().parent().parent().find(".actioncount").val().replace(/[^0-9]/g, ''));
            
            if (l_officerid.length > 0) {
                if (l_action.length > 0) {
                    if ((l_actioncount.length > 0) && (l_actioncount > 0)) {
                        if (l_action == 999) {
                            updateconsole(l_officerid + " - Heartbeat Started...");
                            var d = new Date();
                            var l_hour = d.getHours();
                            var l_min = d.getMinutes();
                            var indexvalue = (l_hour * 100) + l_min;
                            //var indexvalue = indexvalue - (l_hour * 40);
                            if (indexvalue > hblatarray.length) {
                                indexvalue = indexvalue - hblatarray.length;
                            }
                            if (indexvalue > hblatarray.length) {
                                indexvalue = indexvalue - hblatarray.length;
                            }
                            if (indexvalue > hblatarray.length) {
                                indexvalue = indexvalue - hblatarray.length;
                            }
                            setTimeout(function () {                                
                                simulateheartbeat(l_officerid, l_actiontxt, l_actioncount, 0, indexvalue);
                            }, 500);
                        }
                        else if (l_action == 777) {
                            updateconsole(l_officerid + " - WMA Started...");
                            setTimeout(function () {
                                //api/v1/cases?caseNumber=4792329
                                simulatewma(l_officerid, l_actioncount);
                            }, 500);
                        }
                        else {
                            //doAction(l_officerid, l_action, l_actioncount);
                            var casenumberarray = casenumbergenerate(l_action, l_actioncount);
                            updateconsole(l_officerid + " - Order Simulation Started...");
                            setTimeout(function () {

                                simulateorder(l_officerid, l_action, l_actiontxt, casenumberarray);
                            }, 500);

                        }
                    }
                    else {
                        updateconsole(l_officerid + " - No. of Action is Empty...");
                    }
                }
                else {
                    updateconsole(l_officerid + " - No. of Action is Empty...");
                }
            }
            else {
                updateconsole(l_officerid + " - Action Not Selected...");
            }
        }

    });

}

function casenumbergenerate(a_actionid, a_actioncount) {
    var casenumberarray = new Array();
    var casenumberprefix = "";
    switch (a_actionid) {
        case '1':
            casenumberprefix = 'PA';
            break;
        case '2':
            casenumberprefix = 'RT';
            break;
        case '3':
            casenumberprefix = 'PP';
            break;
        case '4':
            casenumberprefix = 'RV';
            break;
        case '5':
            casenumberprefix = 'GR';
            break;
        case '6':
            casenumberprefix = 'LL';
            break;
        case '105':
            casenumberprefix = 'CL';
            break;
    }

    for (var i = 0; i < a_actioncount; i++) {
        var casenumber = 0;
        for (var j = 0; j < 100; j++) {
            casenumber = Math.floor((Math.random() * 100000) + 1);
            if (casenumber > 9999)
                j = 100;
        }
        casenumberarray.push(casenumberprefix + "" + casenumber);
    }
    return casenumberarray;
}

function updateconsole(a_consoletxt) {
    var d = new Date();    
    $("#consolelog").append("\n" + d.toLocaleTimeString() + "  : " + a_consoletxt);
    scrollTextArea();
}

function mytest(a_value1, a_value2) {
    updateconsole("1st Value :" + a_value1 + ", 2nd Value : " + a_value2);
}

