﻿//==========================================================================================================================================
/* File Name: Admin_Custom.js */
/* File Created: Jan 18, 2017 */
/* Created By  : R.Santhakumar */
//==========================================================================================================================================
//TODO : Sevice URL
//var serviceUrl = "https://test-optimise.marstongroup.co.uk:26443/OAServiceV2/MarstonOfficeToolService.svc";
var ServiceURL = "https://test-optimise.marstongroup.co.uk:26443/OptimiseAPI/";

//==========================================================================================================================================
// TODO: Home page link

// TODO: Menu link
//==========================================================================================================================================
var TeamAdminUrl = "http://localhost:8063/Admin/ITadmin.html";
var HrUrl = "http://localhost:8063/Admin/HRView.html";
var ReturnActionUrl = "http://localhost:8063/Admin/PendingAction.html";
var CaseSearchUrl = "http://localhost:8063/Admin/CaseSearch.html";
var ApproveRequestUrl = "http://localhost:8063/Admin/AssignRequest.html";
var HPICheckRequestUrl = "http://localhost:8063/Admin/HPICheckRequest.html";
var PaymentReportUrl = "http://localhost:8063/Admin/Reports.html";
var CaseuploadUrl = "http://localhost:8063/Admin/CaseUpload.html";
var CGAdminUrl = "http://localhost:8063/Admin/CGadmin.html";
var ClampedImageUrl = "http://localhost:8063/Admin/ClampedImage.html";
var BailedImageSearchUrl = "http://localhost:8063/Admin/BailedImage.html";
var CaseReturnUrl = "http://localhost:8063/Admin/CaseReturn.html";
var SCAdminUrl = "http://localhost:8063/Admin/SCadmin.html";
//==========================================================================================================================================
// TODO: Default parameters
var DataType = 'json';
var max;// Notification message character
var OfficerID;// = window.name.toString().split('|');
var Tab, CntTab1, CntTab2, CntTab3;
var date;
var TodayDate;
var TodayDateOnly;
var RefreshIntervalValue;
var counter;
var CaseActionURL = "";
var WMAURL = "", WMAType = "";
var StatsURL = "";
var LocationURL = "";
var RankURL = "", RankActionText = "", RankViewType = "";
var MTV = '';

var serviceTypeLogin;
var GlobalUserName;
var GlobalPassword;

var serviceTypeCaseSearch;
var serviceTypeCaseSearchDetail;
var serviceTypeSearchVRM;
var serviceTypeSearchVRMDetail;

var LinkForManager = 0;
var CompanyID = 1;
var OffID;
var tableIndex = 1; // for officer status tables
var IntervalvalueOfficerStatus;
var MapIntervalStatus;
var MID, FD, TD, OID, GID;
var datacontent, datacontentforDisplay;

var center;
var options;
var markers = [];
var map;
var DMap;
var latLng;
var marker;
var markerCluster;
var infowindow;
var mcOptions;
var Latitude = [];
var Longitude = [];
var Icon = [];
var Content = [];
var pLatitude = 0;
var pLongitude = 0;
var pContentkey;
var pLatMap = 0;
var pLongMap = 0;
var Clamp = 0;
var ClampMgr = 0;
var ClampImg = '';
var ViewZoneMgr;
var Bailed = 0;
var BailedMgr = 0;
var BailedKey, ClampKey;
var LinkForManager = 0;
var Type;
var RequestURL = "";
var counter;
var geocoder;

var entityMap = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#39;',
    '/': '&#x2F;',
    '`': '&#x60;',
    '=': '&#x3D;'
};

//==========================================================================================================================================

$(document).ready(function () {
    $('#officerId,#password').keyup(function (e) {
        if (e.keyCode == 13)
            $('#btnLogin').trigger('click');
    });

    $('#btnLogin').click(function () {
        if ($('#officerId').val() == '') {
            $('#lblUNPWDWarningInfo').html('Whoops! Invalid OfficerId.');
            $(".errormsg").show();
            return;
        }

        if ($('#password').val() == '') {
            $('#lblUNPWDWarningInfo').html('Whoops! Invalid Password.');
            $(".errormsg").show();
            return;
        }
        $('#imgLoginProcessing').show();
        GetToken();
    });

    if ($.session.get("timervalue") >= 0) {
        Timer();
    }

    $(".Numeric").keydown(function (e) {
        $(this).attr('style', 'border-color: none');
        if (e.shiftKey == true) {
            if (e.shiftKey == true && e.keyCode != 16) {
                $(this).attr('style', 'border:2px solid #7A4073');
                $(this).attr('placeholder', 'Enter numeric values');
                return false;
            }
            return false;
        }
        if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 35 || e.keyCode == 36 ||
                     e.keyCode == 37 || e.keyCode == 39 || e.keyCode == 45 || e.keyCode == 46 || e.keyCode == 144 || e.keyCode == 59 || e.ctrlKey == true) {
            $(this).attr('style', 'border-color: none');
            $(this).removeAttr('placeholder');
            return true;
        }
        else {
            $(this).attr('style', 'border:2px solid #7A4073');
            $(this).attr('placeholder', 'Enter numeric values');
            return false;
        }
    });
});

//==========================================================================================================================================
// TODO : Login function

function GetToken() {
    $.ajax({
        type: 'POST',
        url: ServiceURL + 'token',
        data: {
            "grant_type": "password",
            "username": $('#officerId').val(),
            "password": $('#password').val()
        },
        dataType: DataType,
        success: function (data, textStatus, xhr) {
            //$.session.set('Token', data.access_token);
            //$.session.set('TokenType', data.token_type);
            $.session.set('Token', data.access_token);
            $.session.set('ExpiresIn', data.expires_in);
            // $.session.set('issued', data['.issued']);
            //$.session.set('expires', data['.expires']);
            $.session.set('TokenAuthorization', data.token_type + ' ' + data.access_token);
            $.session.set('OfficerID', $.trim($('#officerId').val()));
            $.session.set('Password', $.trim($('#password').val()));
            Loginnew();
        },
        error: function (xhr, textStatus, errorThrown) {
            $('#lblUNPWDWarningInfo').html("Officer ID and Password didnot match.");
        }
    });
}

function Loginnew() {
    $.ajax({
        type: 'POST',
        url: ServiceURL + 'api/v1/officers/' + $.session.get('OfficerID') + '/Login',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: DataType,
        success: function (data) {
            if (!(data == '' || data == null || data == '[]')) {
                $.session.set("timervalue", 0);
                $.session.set('OfficerName', data.OfficerName);
                $.session.set('CompanyID', data.CompanyID);
                //TargetNo missing
                $.session.set('tgVal', data.TargetNo);
                $.session.set('aref', data.AutoRefresh);
                $.session.set('RoleType', data.RoleType);
                $.session.set('OfficerRole', data.OfficerRole);
                if (data.RoleType == 6) {
                    $.session.set('bc', 1);
                    $.session.set('IsVedio', 1);
                    window.top.location.href = HrUrl;// "HRView.html";
                }
                else if (data.RoleType == 5) {
                    $.session.set('bc', 1);
                    $.session.set('IsVedio', 1);
                    window.top.location.href = RedirectUrl;
                }
                else if (data.RoleType == 1) {
                    $.session.set('IsVedio', 1);
                    if (data.CompanyID == 1)
                        window.top.location = "Dashboard_MS.html";
                    else
                        window.top.location = "Dashboard_HC.html";
                }
                else if (data.RoleType == 7) {
                    if (data.CompanyID == 1)
                        window.top.location = "Dashboard_ES.html";
                    else
                        window.top.location = "Dashboard_HC.html";
                }
                else {
                    $.session.set('bc', 1);
                    $.session.set('IsVedio', 1);
                    if (data.CompanyID == 1)
                        window.top.location = "Dashboard_MS.html";
                    else
                        window.top.location = "Dashboard_HC.html";
                }
            }
            else {
                $('#lblUNPWDWarningInfo').html("Officer ID and Password did not match.");
            }
        },
        complete: function () {
        },
        error: function (xhr, textStatus, errorThrown) {
            // alert('Error');
        }
    });
}

function Timer() {
    setInterval(function () {
        var timervalue = parseInt($.session.get("timervalue"));
        timervalue = timervalue + 1;
        $.session.set("timervalue", timervalue);
        if (timervalue >= 15) {
            CheckToken();
        }
    }, 60000);
}

function CheckToken() {

    $.ajax({
        type: 'POST',
        url: ServiceURL + 'token',
        data: {
            "grant_type": "password",
            "username": $.session.get('OfficerID'),
            "password": $.session.get('Password')
        },
        dataType: DataType,
        success: function (data, textStatus, xhr) {
            //console.log("Authorization : " + data.access_token);
            $.session.set('Token', data.access_token);
            $.session.set('TokenAuthorization', data.token_type + ' ' + data.access_token);
            $.session.set("timervalue", 0);
        },
        error: function (xhr, textStatus, errorThrown) {
            //alert('xhr  :' + JSON.stringify(xhr));
            //alert('textstatus : ' + textstatus);
            //alert(errorThrown);
        }
    });
}

//==========================================================================================================================================
// TODO : Logout function

function Logout() {
    $.ajax({
        type: 'PUT',
        url: ServiceURL + 'api/v1/officers/' + $.session.get('OfficerID') + '/Logout',
        headers: {
            "Authorization": $.session.get('TokenAuthorization'),
            'Content-Type': 'application/json'
        },
        dataType: DataType,
        beforeSend: function () {
        },
        success: function (data) {
            if (data == true) {
                $.session.clear();
                window.location.href = HomeUrl;
            }
        },
        complete: function () { },
        error: function (xhr, textStatus, errorThrown) {
        }
    });
}

function LogoutCancel() {
    $('#' + $('#popup3Trigger').attr('aria-describedby')).attr('class', 'popover fade bottom');
}

//==========================================================================================================================================
// TODO : Alert message

function AlertMessage(title, message) {
    $('#DAlert').attr("title", title.toString());
    $('#DAlert').html(message.toString());
    $('#DAlert').dialog({
        modal: true,
        resizable: false,
        width: "25%",
        buttons: {
            "Close": function () {
                $(this).dialog("close");
            }
        }
    });
}

function parameterPush(paramData, paramKey, paramValue) {
    if ($.trim(paramValue) != '') {
        var myObj = jQuery.parseJSON('{ "' + paramKey + '": ' + paramValue + '}');
        $.extend(true, paramData, myObj);
    }
    return paramData;
}

function escapeHtml(string) {
    return String(string).replace(/[&<>"'`=\/]/g, function (s) {
        return entityMap[s];
    });
}